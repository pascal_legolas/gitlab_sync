import unittest

from sync import *

class TestGitlabSync(unittest.TestCase):

  def test_subgroups_to_create_for_empty_groups(self):
    src=[]
    dst=[]
    expected=[]

    actual=subgroups_to_create(src, dst)

    self.assertEqual(expected, actual)

  def test_subgroups_to_create_empty_dst(self):
    src=[fake({'path': 'a'}), fake({'path': 'b'})]
    dst=[]
    expected=['a', 'b']

    actual = subgroups_to_create(src, dst)

    self.assertEqual(expected, actual)

  def test_subgroups_to_create(self):
    src=[fake({'path': 'a'}), fake({'path': 'b'})]
    dst=[fake({'path': 'b'}), fake({'path': 'c'})]
    expected=['a']

    actual = subgroups_to_create(src, dst)

    self.assertEqual(expected, actual)

  def test_match_src_dst_subgroups_ids(self):
    src=[fake({'path': 'a', 'id': 1}), fake({'path': 'b', 'id': 12})]
    dst=[fake({'path': 'b', 'id': 4}), fake({'path': 'c', 'id': 10}), fake({'path': 'a', 'id': 13})]
    expected=[{'src': 12, 'dst': 4},{'src': 1, 'dst': 13}]

    actual = match_src_dst_subgroups_ids(src, dst)

    self.assertEqual(sorted(expected), sorted(actual))

  def test_match_src_dst_projects_url(self):
    src=[fake({'path': 'a', 'ssh_url_to_repo': 'url1'}), fake({'path': 'b', 'ssh_url_to_repo': 'url2'})]
    dst=[fake({'path': 'b', 'ssh_url_to_repo': 'url4'}), fake({'path': 'c', 'ssh_url_to_repo': 'url10'}), fake({'path': 'a', 'ssh_url_to_repo': 'url13'})]
    expected=[{'src': 'url1', 'dst': 'url13'},{'src': 'url2', 'dst': 'url4'}]

    actual = match_src_dst_projects_url(src, dst)

    self.assertEqual(sorted(expected), sorted(actual))

  def assertEmpty(self, list):
    self.assertEqual(0, len(list))

def fake(array):
  return type('obj', (object,), array)()

if __name__ == '__main__':
  unittest.main()
