#!/bin/bash -e

usage() {
  echo $* >&2
  echo "$0 <src remote> <dst remote>" >&2
  echo >&2
  exit 1
}

src="$1"
dst="$2"

[[ -z "$src" ]] && usage missing source remote
[[ -z "$dst" ]] && usage missing destination remote

work_dir=$(mktemp -d)
echo Working directory: $work_dir

(
cd "$work_dir"

set -x
git clone --mirror "$src" to_sync
cd to_sync

git remote add --mirror=push dst "$dst"

git push dst
set +x
)

rm -rf "$work_dir"
