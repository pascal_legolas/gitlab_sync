import argparse
import os
import subprocess

import gitlab

def main():

  parser = argparse.ArgumentParser(description="Synchronize two gitlab groups")
  parser.add_argument('src_group_id', metavar='src_group_id', type=int,
                      help='id of the gitlab group to synchronize from (source)')
  parser.add_argument('dst_group_id', metavar='dst_group_id', type=int,
                      help='id of the gitlab group to synchronize to (destination)')
  
  args = parser.parse_args()
  src_group_id = args.src_group_id
  dst_group_id = args.dst_group_id

  git_src = gitlab.Gitlab.from_config('source', ['python-gitlab.cfg'])
  git_dst = gitlab.Gitlab.from_config('destination', ['python-gitlab.cfg'])

  print("source api      : " + git_src.api_url)
  print("destination api : " + git_dst.api_url)
  print("source group id      : " + str(src_group_id))
  print("destination group id : " + str(dst_group_id))

  src_group=git_src.groups.get(src_group_id)
  dst_group=git_dst.groups.get(dst_group_id)

  sync_groups(git_src, src_group, git_dst, dst_group)

def sync_groups(git_src, src, git_dst, dst):
  print("syncing subgroups src: " + str(src.id) + " dst: " + str(dst.id))
  src_subgroups = src.subgroups.list(all=True)
  dst_subgroups = dst.subgroups.list(all=True)

  for path in subgroups_to_create(src_subgroups, dst_subgroups):
    create_subgroup(git_dst, dst, path) 
  #refresh with newly created groups if any
  dst_subgroups = dst.subgroups.list(all=True)

  for pair in match_src_dst_subgroups_ids(src_subgroups, dst_subgroups):
    sync_groups(git_src, git_src.groups.get(pair['src']),
                git_dst, git_dst.groups.get(pair['dst']))

  src_projects = src.projects.list(all=True)
  dst_projects = dst.projects.list(all=True)

  for path in projects_to_create(src_projects, dst_projects):
    create_project(git_dst, dst, path)
  #refresh with newly created groups if any
  dst_projects = dst.projects.list(all=True)

  for pair in match_src_dst_projects_url(src_projects, dst_projects):
    sync_git_repos(pair['src'], pair['dst'])

def sync_git_repos(src_url, dst_url):
  print("synchronizing git repo " + src_url + " -> " + dst_url)
  git_sync=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'git_sync.sh')
  subprocess.check_call([git_sync, src_url, dst_url])

def subgroups_to_create(src, dst):
  return children_to_create(src, dst)

def projects_to_create(src, dst):
  return children_to_create(src, dst)

def children_to_create(src, dst):
  return [ g.path for g in src if not group_exists(dst, g.path)]

def match_src_dst_subgroups_ids(srcs, dsts):
  return [{'src': src.id, 'dst': next(dst for dst in dsts if dst.path == src.path).id} for src in srcs]

def match_src_dst_projects_url(srcs, dsts):
  return [{'src': src.ssh_url_to_repo, 'dst': next(dst for dst in dsts if dst.path == src.path).ssh_url_to_repo} for src in srcs]

def group_exists(groups, path):
  return any(g.path == path for g in groups)

def create_subgroup(git, group, path):
  print("creating subgroup " + path + " in " + str(group.id))
  git.groups.create({'name': path, 'path': path, 'parent_id': group.id})

def create_project(git, group, path):
  print("creating project " + path + " in " + str(group.id))
  git.projects.create({'name': path, 'path': path, 'namespace_id': group.id})

if __name__ == '__main__': main()
